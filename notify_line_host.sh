#!/bin/bash

LINE_URL="https://api.line.me/v2/bot/message/push"
LINE_TOKEN="INSERT_CHANNEL_ACCESS_TOKEN_HERE"

while getopts 'a:b:c:d:e:f:g:h:i:' opt ; do
  case $opt in
    a) HOSTALIAS=$OPTARG ;;
    b) NOTIFICATIONTYPE=$OPTARG ;;
    c) LONGDATETIME=$OPTARG ;;
    d) CONTACTADDRESS1=$OPTARG ;;
    e) HOSTSTATE=$OPTARG ;;
    f) HOSTOUTPUT=$OPTARG ;;
    g) SERVICEDESC=$OPTARG ;;
    h) SERVICESTATE=$OPTARG ;;
    i) SERVICEOUTPUT=$OPTARG ;;
  esac
done

LINE_TEXT="${HOSTALIAS}\n${HOSTSTATE} (${NOTIFICATIONTYPE})\n${HOSTOUTPUT}"

curl -X POST ${LINE_URL} -H 'Content-Type: application/json' -H "Authorization: Bearer ${LINE_TOKEN}" -d "{\"to\": \"${CONTACTADDRESS1}\", \"messages\": [{\"type\":\"text\", \"text\":\"${LINE_TEXT}\"}]}"
