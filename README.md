**Unfortunately, LY Corporation has decided to suspend LINE Official Accounts registered in the EU, see [this page](https://www.lycbiz.com/jp-en/news/line-official-account/20231225/) for details.**

# LINE Notifications for Nagios

I am using these bash plugins to have Nagios Core send notifications for both hosts and services to a LINE channel using the Messaging API.

## Prerequisites

- LINE Business ID
- LINE Messaging API channel

## Installation & Configuration

### Nagios Commands

```
define command {
    command_name    notify_line_host
    command_line    $USER1$/notify_line_host.sh -a "$HOSTALIAS$" -b "$NOTIFICATIONTYPE$" -c "$LONGDATETIME$" -d "$CONTACTADDRESS1$" -e "$HOSTSTATE$" -f "$HOSTOUTPUT$"
    register        1
}

define command {
    command_name    notify_line_service
    command_line    $USER1$/notify_line_service.sh -a "$HOSTALIAS$" -b "$NOTIFICATIONTYPE$" -c "$LONGDATETIME$" -d "$CONTACTADDRESS1$" -g "$SERVICEDESC$" -h "$SERVICESTATE$" -i "$SERVICEOUTPUT$"
    register        1
}
```

### Nagios Contacts

Relevant configuration:

```
define contact {
    host_notification_commands       notify_line_host
    service_notification_commands    notify_line_service
    address1                         INSERT_RECIPIENT_LINE_ID_HERE
}
```

Ensure host & service notifications are enabled and notification periods are set up for the specified contact.

### Plugins

- Plugin files must be moved to the Nagios plugin directory (`NAGIOS_ROOT/libexec/`).
- For both files, set `LINE_TOKEN` to the channel's long-lived access token. You may customize `LINE_TEXT` to your personal preferences.

## References

See [this page](https://assets.nagios.com/downloads/nagioscore/docs/nagioscore/4/en/macrolist.html) for a detailed overview and explanation of all standard macros available in Nagios Core.
